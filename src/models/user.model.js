import mongoose from "mongoose";
const Schema = mongoose.Schema;

export const schemaModel = {
  username: {
    type: String,
    default: "",
  },
  password: {
    type: String,
    default: "",
  },
};

export const userSchema = new Schema(schemaModel, { timestamps: true });

export default mongoose.model("User", userSchema);
