import multer from "multer";
import config from "$config/index.js";
import util from "util";

const maxSize = 1 * 1024 * 1024; //1MB
const DIR = `./src/${config.uploadsDir}`;
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(" ").join("-");
    cb(null, fileName);
  },
});

let uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single("file");
let uploadFilesMiddleware = util.promisify(uploadFile);
export default uploadFilesMiddleware;
