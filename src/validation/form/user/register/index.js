import validator from "validator";
import isEmpty from "is-empty";
import schemaValidator from "./schema.js";

export default function validateRegisterInput(data) {
  let errors = [];
  const mobileRegex = new RegExp("^(\\+98|0)?9\\d{9}$");
  Object.entries(schemaValidator).map((item) => {
    const key = item[0];
    const value = item[1];
    data[key] = isEmpty(data[key]) ? "" : data[key];
    switch (key) {
      case "username":
        if (!validator.isLength(data[key], { min: 4, max: 30 })) {
          errors.push(value);
        }
        break;
      case "password":
        if (!validator.isLength(data[key], { min: 6, max: 30 })) {
          errors.push(value);
        }
        break;
      case "address":
        if (!validator.isLength(data[key], { min: 4, max: 100 })) {
          errors.push(value);
        }
        break;
      case "phoneNumber":
        if (!mobileRegex.test(data[key])) {
          errors.push(value);
        }
        break;
      default:
        if (validator.isEmpty(data[key])) {
          errors.push(value);
        }
        break;
    }
  });

  return {
    errors,
    isValid: isEmpty(errors),
  };
}
