const schemaValidator = {
  fullName: "پرکردن نام و نام خانواندگی الزامی می باشد",
  username: "نام کاربری باید بیش از ۴ کاراکتر باشد",
  password: "کلمه عبور باید بیش از ۶ کاراکتر باشد",
  phoneNumber: "شماره تلفن وارد شده معتبر نمی باشد",
  province: "پرکردن شهر سکونت الزامی می باشد",
  address: "آدرس کامل را وارد نمایید",
  shippingName: "نام باربری خود را وارد نمایید",
};

export default schemaValidator;
