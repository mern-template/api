import validator from "validator";
import isEmpty from "is-empty";
import schemaValidator from "./schema.js";

export default function validateLoginInput(data) {
  let errors = [];
  Object.entries(schemaValidator).map((item) => {
    const key = item[0];
    const value = item[1];
    data[key] = isEmpty(data[key]) ? "" : data[key];
    switch (key) {
      case "email":
        if (!validator.isLength(data[key], { min: 4, max: 30 })) {
          errors.push(value);
        }
        break;
      case "password":
        if (!validator.isLength(data[key], { min: 6, max: 30 })) {
          errors.push(value);
        }
        break;
      default:
        break;
    }
  });
  return {
    errors,
    isValid: isEmpty(errors),
  };
}
