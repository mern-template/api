import userServices from "$services/user/user.services.js";
import authServices from "$services/auth/auth.services.js";

const services = {
  user: () => userServices,
  auth: () => authServices,
};

export default services;
