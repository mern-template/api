import {
  GET_ALL_USERS,
  GET_SINGLE_USER,
  UPDATE_SINGLE_USER,
  DELETE_SINGLE_USER,
  FIND_USER,
} from "$controllers/user.controller.js";

const services = {
  GET_ALL_USERS: () => GET_ALL_USERS,
  GET_SINGLE_USER: () => GET_SINGLE_USER,
  UPDATE_SINGLE_USER: () => UPDATE_SINGLE_USER,
  DELETE_SINGLE_USER: () => DELETE_SINGLE_USER,
  FIND_USER: () => FIND_USER,
};

export default services;
