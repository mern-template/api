import { REGISTER, LOGIN } from "$controllers/auth.controller.js";

const services = {
  REGISTER: () => REGISTER,
  LOGIN: () => LOGIN,
};

export default services;
