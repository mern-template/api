import dotenv from "dotenv";
import path from "path";
import { fileURLToPath } from "url";
const env = process.env;

if (env.NODE_ENV !== "production") {
  dotenv.config();
}

// add "/.." to get Root => "C:\Users\e.vaziri\Desktop\projects\sbbiran-backend\src"
const rootFilePath = fileURLToPath(`${import.meta.url}/..`);

//paths
const root = path.dirname(rootFilePath); // => /src
const build = path.join(root, "www/index.html"); // => /src/www/index.html
const saved = path.join(root, `${env.UPLOADS_DIR}/`); // => /src/documents/files

//constants
const url = `${env.PROTOCOL}${env.URL}:${env.PORT}`;
const mongodb = env.LOCAL_MODE
  ? `mongodb://${env.URL}/${env.DB}`
  : `mongodb://${env.DATABASE_USER}:${env.DATABASE_PASS}@${env.URL}:27017/${env.DATABASE_NAME}`;

const config = {
  port: env.PORT,
  local_mode: env.LOCAL_MODE,
  secretOrKey: env.SECRET,
  uploadsDir: env.UPLOADS_DIR,
  servedIndex: `${url}/${env.UPLOADS_DIR}`,
  mongodb,
  path: {
    root,
    build,
    saved,
  },
};

export default config;
