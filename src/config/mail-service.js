import config from "$config/index.js";
import nodemailer from "nodemailer";

export const transporter = nodemailer.createTransport({
  host: config.email_host,
  port: 465,
  secure: true,
  auth: {
    user: config.email_user,
    pass: config.email_pass,
  },
});

export const transporterOptions = {
  from: config.email_user,
  to: config.email_destination,
};
