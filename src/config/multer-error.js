const error = {
  LIMIT_PART_COUNT: "فایل ارسال شده نقص دارد",
  LIMIT_FILE_SIZE: "حجم فایل ارسال شده بیشتر از حد مجاز (1 مگابایت) می باشد",
  LIMIT_FILE_COUNT: "تعداد فایل ارسال شده بیشتر از حد مجاز (1 عدد) می باشد",
  LIMIT_FIELD_KEY: "عنوان فایل ارسال شده بسیار طولانی می باشد!!",
  LIMIT_FIELD_VALUE: "Field value too long",
  LIMIT_FIELD_COUNT: "Too many fields",
  LIMIT_UNEXPECTED_FILE: "فیلد غیر منتظره ای همراه با فایل ارسال شده است!",
  MISSING_FIELD_NAME: "فایل ارسال شده فاقد عنوان می باشد",
};

export default error;
