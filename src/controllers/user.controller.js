import UserModel from "$models/user.model.js";
import uploadFilesMiddleware from "$middlewares/upload.middleware.js";
import config from "$config/index.js";
import multerError from "$config/multer-error.js";

export const GET_ALL_USERS = (req, res) => {
  UserModel.find()
    .select("-password")
    .then((data) => {
      res.send(data);
    });
};

export const GET_SINGLE_USER = (req, res) => {
  const id = req.params.id;
  UserModel.findById(id)
    .select("-createdAt -updatedAt -__v -password")
    .then((data) => {
      res.status(200).send(data);
    })
    .catch(() => {
      res.status(404).send({
        message: "an error has been accourd",
      });
    });
};

export const FIND_USER = (req, res) => {
  const obj = req.body;
  UserModel.findOne(obj)
    .then((data) => {
      if (data) {
        res.status(200).json({ message: "exist", data });
      } else {
        res.status(404).json({ message: "dont exist" });
      }
    })
    .catch(() => {
      res.status(404).send({
        message: "an error has been accourd",
      });
    });
};

export const UPDATE_SINGLE_USER = (req, res) => {
  const _id = req.params.id;
  uploadFilesMiddleware(req, res, (err) => {
    const file = req.file;
    if (err) {
      let msg =
        err && err.code && multerError[err.code]
          ? multerError[err.code]
          : "مشکلی در ارتباط با سرور وجود دارد";
      return res.status(400).send({ msg });
    }
    const bodyData = req.body;
    if (file) {
      bodyData.image = `${config.servedIndex.images}/${file.filename}`;
    }
    UserModel.findOneAndUpdate({ _id }, { $set: bodyData }, { new: true })
      .then((data) => {
        if (data !== null) {
          res.status(200).send({
            message: "اطلاعات پروفایل شما با موفقیت ویرایش شد",
            data,
          });
        } else {
          res.status(400).send({
            message: "user was not find to edit",
          });
        }
      })
      .catch((err) => {
        res.status(404).send({
          message: "an error has been accourd",
        });
      });
  });
};

export const DELETE_SINGLE_USER = (req, res) => {
  const _id = req.params.id;
  UserModel.findByIdAndDelete(_id)
    .then((data) => {
      if (data !== null) {
        res.status(200).send({
          message: "user was deleted successfully!",
          data,
        });
      } else {
        res.status(400).send({
          message: "user was not find to delete",
        });
      }
    })
    .catch((data) => {
      res.status(404).send({
        message: "an error has been accourd",
      });
    });
};
