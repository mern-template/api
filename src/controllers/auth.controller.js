import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import UserModel, { schemaModel } from "$models/user.model.js";
import config from "$config/index.js";

export const REGISTER = (req, res) => {
  let value;
  let document = { schemaModel };
  const {
    body: { email },
  } = req;
  UserModel.findOne({ email }).then((user) => {
    if (user) {
      res.statusMessage = "USER_EXIST";
      return res
        .status(401)
        .json({ message: "ایمیل وارد شده قبلا در سیستم ثبت شده است" });
    } else {
      try {
        Object.keys(schemaModel).map((key, index) => {
          switch (key) {
            default:
              value = req.body[key];
              break;
          }
          document[key] = value;
        });
        const newUser = new UserModel(document);

        //createHashedPassword
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then((user) => {
                res.statusMessage = "USER_ADDED";
                res.json({ user, message: "کاربر با موفقیت در سیستم ثبت شد" });
              })
              .catch((err) => console.log(err));
          });
        });
      } catch (error) {
        next(error);
        console.log("error", error);
      }
    }
  });
};

export const LOGIN = async (req, res, next) => {
  const {
    body: { email, password },
  } = req;
  console.log(email);
  var regex = new RegExp("^" + email + "$", "i");
  UserModel.findOne({ email: { $regex: regex } }).then((user) => {
    if (!user) {
      return res
        .status(401)
        .json({ message: "نام کاربری مورد نظر در سیستم ثبت نشده است" });
    }
    bcrypt.compare(password, user.password).then(async (isMatch) => {
      if (isMatch) {
        jwt.sign(
          { id: user._id, role: user.role },
          config.secretOrKey,
          {
            expiresIn: 31556926,
          },
          async (err, token) => {
            res.json({
              token,
            });
          }
        );
      } else {
        return res.status(401).json(["کلمه عبور وارد شده اشتباه است"]);
      }
    });
  });
};
