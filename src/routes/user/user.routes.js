import express from "express";
import passport from "passport";
import services from "$services/index.js";

const router = express.Router();
const userServices = services.user();

router.get(
  "/list",
  passport.authenticate("jwt", { session: false }),
  userServices.GET_ALL_USERS()
);
router.get(
  "/profile/:id",
  passport.authenticate("jwt", { session: false }),
  userServices.GET_SINGLE_USER()
);
router.put(
  "/profile/:id",
  passport.authenticate("jwt", { session: false }),
  userServices.UPDATE_SINGLE_USER()
);
router.delete(
  "/profile/:id",
  passport.authenticate("jwt", { session: false }),
  userServices.DELETE_SINGLE_USER()
);
router.post(
  "/find",
  passport.authenticate("jwt", { session: false }),
  userServices.FIND_USER()
);

export default router;
