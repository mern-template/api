import express from "express";
import services from "$services/index.js";

const router = express.Router();
const authServices = services.auth();

router.post("/register", authServices.REGISTER());
router.post("/login", authServices.LOGIN());

export default router;
