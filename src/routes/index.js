import express from "express";
import authRoutes from "$routes/auth/auth.routes.js";
import userRoutes from "$routes/user/user.routes.js";

const appRouter = express();

appRouter.use("/auth", authRoutes);
appRouter.use("/users", userRoutes);

export default appRouter;
