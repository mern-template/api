# MERN - Template - API

This is a simple, lightweight and fast API template that youcan clone and start using it.

## Table of contents

- [Getting started]("#getting-started")
- [Installation]("#installation")
- [Development]("#development")
- [Run]("#run")

## Getting started

To get started, you need these items installed in your os.

- Git
- NodeJs
- NPM
- Nodemon

## Installation

First, install them on your system. if you are using windows, checkout the home pages to download, but if you are in Linux, follow these instructures:

- ### Ubuntu / Debian

```bash
$ sudo apt install git nodejs npm
```

- ### Manjaro / Arch

```bash
$ sudo pacman -S git nodejs npm
```

- ### CentOS / Fedora

```bash
$ sudo dnf install git nodejs npm
```

Now install `nodemon` to serve your application.

```bash
$ npm i -g nodemon
```

## Development

So, now do steps below:

- Clone

```bash
$ git clone https://gitlab.com/mern-template/api <name>
$ cd <name>
```

- Install dependencies

```bash
# Using npm
$ npm i

# Using yarn
$ yarn install
```

- Create your `.env` file and fill them all

```bash
$ cp .env.example .env
```

## Run

Right, now start your app.

```bash
$ npm test
```