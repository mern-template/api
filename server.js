//packages
import express from "express";
import morgan from "morgan";
import mongoose from "mongoose";
import serveIndex from "serve-index";
import passport from "passport";
import cors from "cors";

//config
import config from "$config/index.js";
import initialePassport from "$config/passport.js";

//components
import appRouter from "$routes/index.js";

const app = express();

mongoose.connect(config.mongodb, (err) => {
  if (err) {
    console.log(err);
    console.log("error in connected to mongoDB");
  } else {
    console.log("connected to MongoDB successfully");
  }
});

app.use(morgan("combined"));
app.use(express.json());
app.use(
  express.urlencoded({
    limit: "10mb",
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(
  "/documents",
  express.static("src/documents"),
  serveIndex("src/documents", { icons: true })
);

app.use(
  cors({
    origin: "*",
    credentials: true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  })
);

app.use(passport.initialize());
initialePassport(passport);
app.use("/v1", appRouter);

console.log(config);
app.listen(config.port);
